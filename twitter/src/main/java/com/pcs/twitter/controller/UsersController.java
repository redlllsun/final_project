package com.pcs.twitter.controller;

import com.pcs.twitter.forms.UserForm;
import com.pcs.twitter.models.Post;
import com.pcs.twitter.models.User;
import com.pcs.twitter.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class UsersController {

    private final UserService userService;

    @Autowired
    public UsersController(UserService userService) {

        this.userService = userService;
    }

    @GetMapping("/users")
    public String getUsersPage(Model model) {
        List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/users/{user-id}")
    public String getUserPage(Model model, @PathVariable("user-id") Integer userId) {
        User user = userService.getUser(userId);
        model.addAttribute("user", user);
        return "user";
    }

    @PostMapping("/users")
    public String addUser(UserForm form) {
        userService.addUser(form);
        return "redirect:/users";
    }

    //POST localhost/users/2/delete
    @PostMapping("/user/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Integer userId) {
        userService.deleteUser(userId);
        return "redirect:/users";
    }

    @PostMapping("/users/{user-id}/update")
    public String updateUser(@PathVariable("user-id") Integer userId, UserForm userForm) {
        userService.updateUser(userId, userForm);
        return "redirect:/users/" + userId;
    }

    @GetMapping("/users/{user-id}/post")
    public String getPostByUser(Model model, @PathVariable("user-id") Integer userId) {
        List<Post> post = userService.getPostByUser(userId);
        model.addAttribute("post", post);
        return "post_of_user";
    }

}


