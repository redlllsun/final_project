package com.pcs.twitter.forms;

import lombok.Data;

@Data
public class UserForm {

    private String first_name;
    private String last_name;
    private String nickname;
    private String email;
}
