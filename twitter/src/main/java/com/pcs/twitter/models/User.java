package com.pcs.twitter.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Setter
@Getter
@Table(name = "users_add")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(columnDefinition = "String not null")
    private String first_name;

    @Column(columnDefinition = "String not null")
    private String last_name;

    @Column(columnDefinition = "String not null")
    private String nickname;

    @Column(columnDefinition = "String not null")
    private String email;

    @OneToMany(mappedBy = "author")
    private List<Post> post;

    public static final class Fields {

        public static final String ID = "id";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String NICKNAME = "nickname";
        public static final String EMAIL = "email";
        public static final String POST = "post";
    }
}
