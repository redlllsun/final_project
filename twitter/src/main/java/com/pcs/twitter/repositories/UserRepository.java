package com.pcs.twitter.repositories;

import com.pcs.twitter.models.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Integer> {
}
