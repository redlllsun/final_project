//package com.pcs.twitter.repositories;
//
//
//import com.pcs.twitter.models.User;
//import com.pcs.twitter.repositories.oldrep.UsersRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Component;
//
//import javax.sql.DataSource;
//import java.util.List;
//
//@Component
//public class UsersRepositoryTemplateImpl implements UsersRepository {
//    //language=SQL
//    private static final String SQL_INSERT = "insert into users_add(first_name, last_name, nickname, email) values(?, ?, ?, ?)";
//    //language=SQL
//    private static final String SQL_SELECT_ALL = "select * from users_add order by id";
//    //language=SQL
//    private static final String SQL_DELETE_BY_ID = "delete from users_add where id=?";
//    //language=SQL
//    private static final String SQL_SELECT_BY_ID = "select * from users_add where id=?";
//    //language=SQL
//    private static final String SQL_UPDATE_BY_ID = "update users_add set first_name=?, last_name=?, nickname=?, email=? ";
//
//    private final JdbcTemplate jdbcTemplate;
//
//    @Autowired
//    public UsersRepositoryTemplateImpl(DataSource dataSource) {
//        this.jdbcTemplate = new JdbcTemplate(dataSource);
//    }
//
//    private static final RowMapper<User> userRowMapper = (row, rowNumber) -> {
//        int id = row.getInt("id");
//        String firstname = row.getString("first_name");
//        String lastname = row.getString("last_name");
//        String nickname = row.getString("nickname");
//        String email = row.getString("email");
//
//        return new User(id, firstname, lastname, nickname, email);
//    };
//    @Override
//    public List<User> findAll() {
//        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
//    }
//    @Override
//    public void save(User user) {
//        jdbcTemplate.update(SQL_INSERT,
//                user.getFirst_name(),
//                user.getLast_name(),
//                user.getNickname(),
//                user.getEmail());
//    }
//
//    @Override
//    public void delete(Long userId) {
//        jdbcTemplate.update(SQL_DELETE_BY_ID, userId);
//    }
//
//    @Override
//    public User findById(Long userId) {
//        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, userRowMapper, userId);
//    }
//
//    @Override
//    public User getById(Long userId) {
//        return jdbcTemplate.queryForObject(SQL_UPDATE_BY_ID, userRowMapper, userId);
//    }
//
//}
