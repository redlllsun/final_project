package com.pcs.twitter.services;

import com.pcs.twitter.forms.UserForm;
import com.pcs.twitter.models.Post;
import com.pcs.twitter.models.User;

import java.util.List;

public interface UserService {

    User addUser(UserForm form);

    List<User> getAllUsers();

    void deleteUser(Integer userId);

    User getUser(Integer userId);

    void updateUser(Integer userId, UserForm userForm);

    List<Post> getPostByUser(Integer userId);
}
