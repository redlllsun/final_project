package com.pcs.twitter.services;

import com.pcs.twitter.exeptions.UserNotFoundExeption;
import com.pcs.twitter.forms.UserForm;
import com.pcs.twitter.models.Post;
import com.pcs.twitter.models.User;
import com.pcs.twitter.repositories.PostRepository;
import com.pcs.twitter.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PostRepository postRepository;

//    @Autowired
//    public UserServiceImpl(UserRepository userRepository, PostRepository postRepository) {
//
//        this.userRepository = userRepository;
//        this.postRepository = postRepository;
//    }

    @Override
    public User addUser(UserForm form) {
        User user = User.builder()
                .first_name(form.getFirst_name())
                .last_name(form.getLast_name())
                .nickname(form.getNickname())
                .email(form.getEmail())
                .build();
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void deleteUser(Integer userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public User getUser(Integer userId) {
        return userRepository.findById(userId).orElseThrow(UserNotFoundExeption::new);
    }

    @Override
    public void updateUser(Integer userId, UserForm userForm) {
        User user = userRepository.getById(userId);
        user.setFirst_name(userForm.getFirst_name());
        user.setLast_name(userForm.getLast_name());
        user.setNickname(userForm.getNickname());
        userRepository.save(user);
    }

    @Override
    public List<Post> getPostByUser(Integer userId) {
        return postRepository.findAllByAuthor(userId);
    }

}
