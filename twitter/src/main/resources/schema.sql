CREATE TABLE users_add
(
    id         SERIAL PRIMARY KEY,
    first_name VARCHAR,
    last_name  VARCHAR,
    nickname   VARCHAR,
    email      VARCHAR
);

INSERT INTO users_add(first_name, last_name, nickname, email)
VALUES ('Мария', 'Иванова', 'nick', 'test@test.ru');
INSERT INTO users_add(first_name, last_name, nickname, email)
VALUES ('Виолета', 'Пупкина', 'nick1', 'test1@test1.ru');
INSERT INTO users_add(first_name, last_name, nickname, email)
VALUES ('Васисуалий', 'Пупкин', 'nick2', 'test2@test2.ru');
INSERT INTO users_add(first_name, last_name, nickname, email)
VALUES ('Инокентий', 'Васечкин', 'nick3', 'test3@test3.ru');
INSERT INTO users_add(first_name, last_name, nickname, email)
VALUES ('Зимовей', 'Зимкин', 'nick4', 'test4@test4.ru');