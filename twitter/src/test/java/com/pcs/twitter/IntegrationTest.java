package com.pcs.twitter;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.sql.DataSource;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SpringBootTest
@Transactional
@Rollback
@Sql(scripts = "classpath:schema.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Import(IntegrationTest.TestContainersConfig.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface IntegrationTest {


    @TestConfiguration
    class TestContainersConfig {

        @Bean(initMethod = "start", destroyMethod = "stop")
        public PostgreSQLContainer postgres() {
            return new PostgreSQLContainer<>("postgres");
        }


        @Bean
        @Primary
        public DataSource dataSource() {
            return DataSourceBuilder.create()
                    .url(postgres().getJdbcUrl())
                    .driverClassName(postgres().getDriverClassName())
                    .username(postgres().getUsername())
                    .password(postgres().getPassword())
                    .build();
        }
    }
}
