package com.pcs.twitter;

import com.pcs.twitter.forms.UserForm;
import com.pcs.twitter.models.User;
import com.pcs.twitter.services.UserService;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.assertj.core.api.Assertions.assertThat;

@IntegrationTest
public class UsersIntegrationTest {


    @Autowired
    private UserService userService;

    @Test
    public void whenUserFormContainsAllNonNullFields_addUser_shouldCreateUserWithMandatoryFields() {
        //given
        EasyRandom easyRandom = new EasyRandom();
        UserForm expected = easyRandom.nextObject(UserForm.class);

        //when
        User actual = userService.addUser(expected);

        //then
        assertThat(actual).usingRecursiveComparison()
                .ignoringFields(User.Fields.ID, User.Fields.POST)
                .isEqualTo(expected);

    }
}
