package com.pcs.twitter.controller;

import com.pcs.twitter.models.User;
import com.pcs.twitter.services.UserService;
import lombok.SneakyThrows;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(UsersController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    @SneakyThrows
    public void whenUsersExist_getUsersPage_shouldAddThemToModelAndRenderUsersView() {
        User user_1 = createRandomUser();
        User user_2 = createRandomUser();
        List<User> expectedUsers = Arrays.asList(user_1, user_2);
        when(userService.getAllUsers()).thenReturn(expectedUsers);

        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("users", hasSize(expectedUsers.size())))
                .andExpect(model().attribute("users", hasItems(
                        allOf(
                                hasProperty(User.Fields.FIRST_NAME, is(user_1.getFirst_name())),
                                hasProperty(User.Fields.LAST_NAME, is(user_1.getLast_name())),
                                hasProperty(User.Fields.EMAIL, is(user_1.getEmail())),
                                hasProperty(User.Fields.NICKNAME, is(user_1.getNickname()))),
                        allOf(
                                hasProperty(User.Fields.FIRST_NAME, is(user_2.getFirst_name())),
                                hasProperty(User.Fields.LAST_NAME, is(user_2.getLast_name())),
                                hasProperty(User.Fields.EMAIL, is(user_2.getEmail())),
                                hasProperty(User.Fields.NICKNAME, is(user_2.getNickname()))))))
                .andExpect(view().name("users"));
    }

    private User createRandomUser() {
        return new EasyRandom().nextObject(User.class);
    }

}
