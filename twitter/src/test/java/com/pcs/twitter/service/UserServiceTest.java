package com.pcs.twitter.service;

import com.pcs.twitter.forms.UserForm;
import com.pcs.twitter.models.User;
import com.pcs.twitter.repositories.UserRepository;
import com.pcs.twitter.services.UserServiceImpl;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.jeasy.random.FieldPredicates.named;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Captor
    private ArgumentCaptor<User> userArgumentCaptor;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void whenUserFormContainsAllNonNullFields_addUser_shouldCreateUserWithMandatoryFields() {
        //given
        EasyRandom easyRandom = new EasyRandom();
        UserForm expected = easyRandom.nextObject(UserForm.class);

        //when
        userService.addUser(expected);
        verify(userRepository).save(userArgumentCaptor.capture());

        //then
        assertThat(userArgumentCaptor.getValue()).usingRecursiveComparison()
                .ignoringFields(User.Fields.ID, User.Fields.POST)
                .isEqualTo(expected);
    }

    @Test
    public void whenUserFormContainsNullFirstName_addUser_shouldCreateUserWithMandatoryFields() {
        //given
        EasyRandom easyRandom = new EasyRandom(new EasyRandomParameters()
                .randomize(named(User.Fields.FIRST_NAME), () -> null));
        UserForm expected = easyRandom.nextObject(UserForm.class);

        //when
        userService.addUser(expected);
        verify(userRepository).save(userArgumentCaptor.capture());

        //then
        assertThat(userArgumentCaptor.getValue()).usingRecursiveComparison()
                .ignoringFields(User.Fields.ID, User.Fields.POST)
                .isEqualTo(expected);
    }

}
